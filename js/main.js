//Теория
// Экранирование - это возможность вывода определенных выражений на экран, т.е. если мы хотим вывести на экран какой-нибудь спецсимвол, который в языке выполняет какое-то действие, или иконку копирайта в юникод формате, то мы можем применить к нему бэкслеш (\): \", \‎u00A9 и т.д.

//Задание

function createNewUser() {
    return {
        firstName: prompt('Enter your name:'),
        lastName: prompt('Enter you surname:'),
        birthday: prompt('Enter your birthdate: dd.mm.yyyy'),
        getAge: function(date, result) {
            date = this.birthday.split('.').reverse().join('-');
            result = ((new Date().getTime() - new Date(date)) / (24 * 3600 * 365.25 * 1000));
            return parseInt(result);
        },
        getLogin: function() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getPassword: function() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.split('.')[2];
        },
    };
}

const newUser = createNewUser();

console.log( newUser );
console.log( newUser.getAge() );
console.log( newUser.getPassword() );
